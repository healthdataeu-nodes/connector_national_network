# Connector National network

### description

This projetct serves the purpose of emulating a national connector to send, update and delete metadata records on [**HDCentralModules**](https://code.europa.eu/healthdataeu-nodes/hdcentralmodules). Also will receive and handle data acces and data permit requests.

### dependencies

HDEUPOC images and components.

## create the containers

using the following command line

```shell
docker-compose -f docker-compose.yml up --build -d
```

Domibus instances is only ready after you see the following line:

```shell
INFO [main] org.apache.catalina.startup.Catalina.start Server startup in [38647] milliseconds
```

## Configuration

### change default password:

After that you should go to the browser and change the default admin password:

* Go to central services:
  * http://localhost:6080/domibus/
    user: admin
    pass: 123456

### deploy keys and properties

In the command line, run the following script:

```shell
./deploy-keys-and-properties.sh
```

## Reload truststores

Reload the trustore using the web admin console of Domibus

### go to: http://localhost:6080/domibus/

In the left menu, navigate to “Trustores” -> “Domibus”

* Click on “Upload” and choose
  * ./key-pt-node/gateway_truststore.jks
* Enter the password "test123"
* Press the button Reload KeyStore in the lower right corner. You should see the message Keystore was successfully reset

In the left menu, navigate to “PMode” -> “Current” page

* Upload the PMode file using a description, locate in:
  * ./key-pt-node/PMode-pt-node.xml
* click on “Save” to activate the new PMode file.

#### Create an API user

In the menu bar, navigate to “Plugin User”.

* Create a new plugin user by clicking on “New”
* Select the role: ROLE_ADMIN

NOTE : This user name and password will be used while sending a message through APIs.

